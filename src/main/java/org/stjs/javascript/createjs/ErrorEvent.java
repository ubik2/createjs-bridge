package org.stjs.javascript.createjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class ErrorEvent extends Event {
    public Object data;
    public String message;
    public String title;
    public ErrorEvent() {
        super("error");
        /* compiled code */
    }
    public ErrorEvent(String title) {
        super("error");
        /* compiled code */
    }
    public ErrorEvent(String title, String message) {
        super("error");
        /* compiled code */
    }
    public ErrorEvent(String title, String message, Object data) {
        super("error");
        /* compiled code */
    }
}
