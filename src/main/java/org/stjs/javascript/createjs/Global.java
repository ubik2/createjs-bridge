package org.stjs.javascript.createjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.functions.Callback0;
import org.stjs.javascript.functions.Callback1;
import org.stjs.javascript.functions.Callback2;
import org.stjs.javascript.functions.Callback3;
import org.stjs.javascript.functions.Callback4;
import org.stjs.javascript.functions.Function0;
import org.stjs.javascript.functions.Function1;
import org.stjs.javascript.functions.Function2;
import org.stjs.javascript.functions.Function3;
import org.stjs.javascript.functions.Function4;

@STJSBridge
@Namespace("createjs")
public class Global {
    @SyntheticType
    public static class BrowserDetect {
        public boolean isFirefox;
        public boolean isOpera;
        public boolean isChrome;
        public boolean isIOS;
        public boolean isAndroid;
        public boolean isBlackberry;
    }

    public static BrowserDetect BrowserDetect;

    public static boolean definePropertySupported;
    public static native Class extend(Class subclass, Class superclass);
    public static native int indexOf(Array<? extends Object> array, Object searchElement);
    public static native Class promote(Class subclass, String prefix);
    public static native Callback0 proxy(Callback0 method, Object scope);
    public static native Callback0 proxy(Callback1<Object> method, Object scope, Object arg1);
    public static native Callback0 proxy(Callback2<Object, Object> method, Object scope, Object arg1, Object arg2);
    public static native Callback0 proxy(Callback3<Object, Object, Object> method, Object scope, Object arg1, Object arg2, Object arg3);
    public static native Callback0 proxy(Callback4<Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2, Object arg3, Object arg4);
    public static native Callback1<Object> proxy(Callback1<Object> method, Object scope);
    public static native Callback1<Object> proxy(Callback2<Object, Object> method, Object scope, Object arg1);
    public static native Callback1<Object> proxy(Callback3<Object, Object, Object> method, Object scope, Object arg1, Object arg2);
    public static native Callback1<Object> proxy(Callback4<Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2, Object arg3);
    public static native Callback2<Object, Object> proxy(Callback2<Object, Object> method, Object scope);
    public static native Callback2<Object, Object> proxy(Callback3<Object, Object, Object> method, Object scope, Object arg1);
    public static native Callback2<Object, Object> proxy(Callback4<Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2);
    public static native Callback3<Object, Object, Object> proxy(Callback3<Object, Object, Object> method, Object scope);
    public static native Callback3<Object, Object, Object> proxy(Callback4<Object, Object, Object, Object> method, Object scope, Object arg1);
    public static native Callback4<Object, Object, Object, Object> proxy(Callback4<Object, Object, Object, Object> method, Object scope);
    public static native Function0<Object> proxy(Function0<Object> method, Object scope);
    public static native Function0<Object> proxy(Function1<Object, Object> method, Object scope, Object arg1);
    public static native Function0<Object> proxy(Function2<Object, Object, Object> method, Object scope, Object arg1, Object arg2);
    public static native Function0<Object> proxy(Function3<Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2, Object arg3);
    public static native Function0<Object> proxy(Function4<Object, Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2, Object arg3, Object arg4);
    public static native Function1<Object, Object> proxy(Function1<Object, Object> method, Object scope);
    public static native Function1<Object, Object> proxy(Function2<Object, Object, Object> method, Object scope, Object arg1);
    public static native Function1<Object, Object> proxy(Function3<Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2);
    public static native Function1<Object, Object> proxy(Function4<Object, Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2, Object arg3);
    public static native Function2<Object, Object, Object> proxy(Function2<Object, Object, Object> method, Object scope);
    public static native Function2<Object, Object, Object> proxy(Function3<Object, Object, Object, Object> method, Object scope, Object arg1);
    public static native Function2<Object, Object, Object> proxy(Function4<Object, Object, Object, Object, Object> method, Object scope, Object arg1, Object arg2);
    public static native Function3<Object, Object, Object, Object> proxy(Function3<Object, Object, Object, Object> method, Object scope);
    public static native Function3<Object, Object, Object, Object> proxy(Function4<Object, Object, Object, Object, Object> method, Object scope, Object arg1);
    public static native Function4<Object, Object, Object, Object, Object> proxy(Function4<Object, Object, Object, Object, Object> method, Object scope);
}
