package org.stjs.javascript.createjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.functions.Callback1;

@STJSBridge
@Namespace("createjs")
public class EventDispatcher {
    public EventDispatcher() { /* compiled code */ }
    public static native void initialize(Object target);
    public native Callback1<Event> addEventListener(String type, Callback1<Event> listener);
    public native Callback1<Event> addEventListener(String type, Callback1<Event> listener, boolean useCapture);
    public native boolean dispatchEvent(Event event);
    public native boolean hasEventListener(String type);
    public native void off(String type, Callback1<Event> listener);
    public native void off(String type, Callback1<Event> listener, boolean useCapture);
    public native Callback1<Event> on(String type, Callback1<Event> listener);
    public native Callback1<Event> on(String type, Callback1<Event> listener, Object scope);
    public native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once);
    public native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once, Object data);
    public native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once, Object data, boolean useCapture);
    public native void removeAllEventListeners();
    public native void removeAllEventListeners(String type);
    public native void removeEventListener(String type, Callback1<Event> listener);
    public native void removeEventListener(String type, Callback1<Event> listener, boolean useCapture);
    public native boolean willTrigger(String type);
}
