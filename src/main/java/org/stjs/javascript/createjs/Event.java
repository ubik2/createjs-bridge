package org.stjs.javascript.createjs;

import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Event {
    public boolean bubbles;
    public boolean cancelable;
    public Object currentTarget;
    public boolean defaultPrevented;
    public int eventPhase;
    public boolean immediatePropagationStopped;
    public boolean propagationStopped;
    public boolean removed;
    public Object target;
    public long timeStamp;
    public String type;
    public Event(String type) { /* compiled code */ }
    public Event(String type, boolean bubbles) { /* compiled code */ }
    public Event(String type, boolean bubbles, boolean cancelable) { /* compiled code */ }
    public native Event clone();
    public native void preventDefault();
    public native void remove();
    public native Event set(Map<String, Object> props);
    public native void stopImmediatePropagation();
    public native void stopPropagation();
}
