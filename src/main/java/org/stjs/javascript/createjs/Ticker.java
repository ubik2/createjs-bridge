package org.stjs.javascript.createjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.functions.Callback1;

@STJSBridge
@Namespace("createjs")
public class Ticker /* extends EventDispatcher */ {
    public static double framerate;
    public static long interval;
    public static long maxDelta;
    public static boolean paused;
    public static String RAF;
    public static String RAF_SYNCHED;
    public static String TIMEOUT;
    public static String timingMode;
    public static native long getEventTime();
    public static native long getEventTime(boolean runTime);
    public static native int getMeasuredFPS();
    public static native int getMeasuredFPS(long ticks);
    public static native long getMeasuredTickTime();
    public static native long getMeasuredTickTime(long ticks);
    public static native long getTicks(boolean pauseable);
    public static native long getTime();
    public static native long getTime(boolean runTime);
    public static native void init();
    public static native void reset();

    // Static variants of the EventDispatcher methods
    public static native void initialize(Object target);
    public static native Callback1<Event> addEventListener(String type, Callback1<Event> listener);
    public static native Callback1<Event> addEventListener(String type, Callback1<Event> listener, boolean useCapture);
    public static native boolean dispatchEvent(Event event);
    public static native boolean hasEventListener(String type);
    public static native void off(String type, Callback1<Event> listener);
    public static native void off(String type, Callback1<Event> listener, boolean useCapture);
    public static native Callback1<Event> on(String type, Callback1<Event> listener);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once, Object data);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once, Object data, boolean useCapture);
    public static native void removeAllEventListeners();
    public static native void removeAllEventListeners(String type);
    public static native void removeEventListener(String type, Callback1<Event> listener);
    public static native void removeEventListener(String type, Callback1<Event> listener, boolean useCapture);
    public static native boolean willTrigger(String type);
}