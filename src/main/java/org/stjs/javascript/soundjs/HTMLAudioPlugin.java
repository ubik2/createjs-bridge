package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.dom.Audio;

@STJSBridge
@Namespace("createjs")
public class HTMLAudioPlugin extends AbstractPlugin {
    @SyntheticType
    public static class TagEntry {
        public Audio tag;
        public int numChannels;
    }
    public int defaultNumChannels;
    public static int MAX_INSTANCES;
    public HTMLAudioPlugin() { /* compiled code */ }
    public static native boolean isSupported();
}
