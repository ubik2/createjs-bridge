package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.preloadjs.AbstractLoader;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class FlashAudioLoader extends AbstractLoader {
    public String flashId;

    public native void setFlash(Object flash);
}
