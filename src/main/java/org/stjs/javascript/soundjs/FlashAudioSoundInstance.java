package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class FlashAudioSoundInstance extends AbstractSoundInstance {
    public FlashAudioSoundInstance(String src, long startTime, long duration, Object playbackResource) {
        super(src, startTime, duration, playbackResource);
        /* compiled code */
    }
}
