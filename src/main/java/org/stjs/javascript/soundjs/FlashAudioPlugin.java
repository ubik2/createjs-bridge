package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class FlashAudioPlugin extends AbstractPlugin {
    public boolean flashReady;
    public boolean showOutput;
    public static String swfPath;
    public FlashAudioPlugin() { /* compiled code */ }
    public static native boolean isSupported();
}
