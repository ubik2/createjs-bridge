package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.createjs.EventDispatcher;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class AbstractSoundInstance extends EventDispatcher {
    public long duration;
    public int loop;
    public boolean muted;
    public double pan;
    public boolean paused;
    public Object playbackResource;
    public String playState;
    public long position;
    public String src;
    public Object uniqueId; // string or number
    public double volume;

    public AbstractSoundInstance(String src, long startTime, long duration, Object playbackResource) { /* compiled code */ }
    public native void destroy();
    public native long getDuration();
    public native int getLoop();
    public native boolean getMuted();
    public native double getPan();
    public native boolean getPaused();
    public native Object getPlaybackResource();
    public native long getPosition();
    public native double getVolume();
    public native AbstractSoundInstance play();
    public native AbstractSoundInstance play(Sound.PlayOptions options); // currently js ignores the delay prop
    public native AbstractSoundInstance play(String interrupt);
    public native AbstractSoundInstance play(String interrupt, long delay);
    public native AbstractSoundInstance play(String interrupt, long delay, long offset);
    public native AbstractSoundInstance play(String interrupt, long delay, long offset, int loop);
    public native AbstractSoundInstance play(String interrupt, long delay, long offset, int loop, double volume);
    public native AbstractSoundInstance play(String interrupt, long delay, long offset, int loop, double volume, double pan);
    public native AbstractSoundInstance setDuration(long value);
    public native AbstractSoundInstance setLoop(int value);
    public native AbstractSoundInstance setMuted(boolean value);
    public native AbstractSoundInstance setPan(double value);
    public native AbstractSoundInstance setPaused(boolean value);
    public native AbstractSoundInstance getPlaybackResource(Object playbackResource);
    public native AbstractSoundInstance setPosition(long value);
    public native AbstractSoundInstance setVolume(double value);
    public native AbstractSoundInstance stop();
}
