package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class WebAudioSoundInstance extends AbstractSoundInstance {
    public static Object context; // AudioContext
    public static Object destinationNode; // AudioNode
    public Object gainNode; // AudioGainNode
    public Object panNode; // AudioPannerNode
    public Object sourceNode; // AudioNode

    public WebAudioSoundInstance(String src, long startTime, long duration, Object playbackResource) {
        super(src, startTime, duration, playbackResource);
        /* compiled code */
    }
}
