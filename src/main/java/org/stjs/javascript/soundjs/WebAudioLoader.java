package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.preloadjs.AbstractLoader;
import org.stjs.javascript.preloadjs.LoadItem;
import org.stjs.javascript.preloadjs.XHRRequest;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class WebAudioLoader extends XHRRequest {
    public Object context; // AudioContext
    public WebAudioLoader(LoadItem loadItem) {
        super(loadItem, true, AbstractLoader.SOUND);
        /* compiled code */
    }
}
