package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.preloadjs.AbstractLoader;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class AbstractPlugin {
    public AbstractPlugin() { /* compiled code */ }
    public native AbstractSoundInstance create(String src, long startTime, long duration );
    public native double getVolume();
    public native boolean isPreloadComplete(String src);
    public native boolean isPreloadStarted(String src);
    public native Object preload(AbstractLoader loader);
    public native Object register(String loadItem, int instances);
    public native void removeAllSounds(String src);
    public native void removeSound(String src);
    public native boolean setMute(boolean value);
    public native boolean setVolume(double value);
}
