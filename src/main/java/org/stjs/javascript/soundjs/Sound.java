package org.stjs.javascript.soundjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.createjs.Event;
import org.stjs.javascript.functions.Callback1;
import org.stjs.javascript.preloadjs.LoadItem;

@STJSBridge
@Namespace("createjs")
public class Sound /* extends EventDispatcher */ {
    @SyntheticType
    public static class PlayOptions {
        public long delay;
        public long duration;
        public String interrupt;
        public int loop;
        public long offset;
        public double pan;
        public long startTime;
        public double volume;
    }
    @SyntheticType
    public static class SoundEntry {
        public String src;
        public String id;
        public Object data;
        public String type;
        public String name;
        public String extension;
        public String path;
        // tag, completeHandler, type
    }
    public static Object activePlugin;
    public static Array<String> alternateExtensions;
    public static String defaultInterruptBehavior;
    public static Map<String, String> EXTENSION_MAP;
    public static String INTERRUPT_ANY;
    public static String INTERRUPT_EARLY;
    public static String INTERRUPT_LATE;
    public static String INTERRUPT_NONE;
    public static String PLAY_FAILED;
    public static String PLAY_FINISHED;
    public static String PLAY_INITED;
    public static String PLAY_INTERRUPTED;
    public static String PLAY_SUCCEEDED;
    public static Array<String> SUPPORTED_EXTENSIONS;

    public static native AbstractSoundInstance createInstance(String src);
    public static native AbstractSoundInstance createInstance(String src, long startTime);
    public static native AbstractSoundInstance createInstance(String src, long startTime, long duration);
    public static native Map<String, Object> getCapabilities();
    public static native Object getCapability(String key);
    public static native boolean getMute();
    public static native double getVolume();
    public static native boolean initializeDefaultPlugins();
    public static native boolean isReady();
    public static native boolean loadComplete(String src);
    public static native AbstractSoundInstance play(String src);
    public static native AbstractSoundInstance play(String src, Object options);
    public static native AbstractSoundInstance play(String src, Object options, long delay);
    public static native AbstractSoundInstance play(String src, Object options, long delay, long offset);
    public static native AbstractSoundInstance play(String src, Object options, long delay, long offset, int loop);
    public static native AbstractSoundInstance play(String src, Object options, long delay, long offset, int loop, double volume);
    public static native AbstractSoundInstance play(String src, Object options, long delay, long offset, int loop, double volume, double pan);
    public static native AbstractSoundInstance play(String src, Object options, long delay, long offset, int loop, double volume, double pan, long startTime);
    public static native AbstractSoundInstance play(String src, Object options, long delay, long offset, int loop, double volume, double pan, long startTime, long duration);
    public static native Array<SoundEntry> registerManifest(Array<LoadItem> manifest, String basePath);
    public static native boolean registerPlugins(Array<Class> plugins);
    // TODO: this cluster of register methods really returns true, false, or object
    public static native LoadItem registerSound(LoadItem obj);
    public static native LoadItem registerSound(LoadItem obj, String dummyId, int dummyData, String basePath);
    public static native LoadItem registerSound(String src);
    public static native LoadItem registerSound(String src, String id);
    public static native LoadItem registerSound(String src, String id, int data);
    public static native LoadItem registerSound(String src, String id, int data, String basePath);
    public static native LoadItem registerSound(String src, String id, Object data);
    public static native LoadItem registerSound(String src, String id, Object data, String basePath);
    public static native Array<LoadItem> registerSounds(Array<LoadItem> sounds, String basePath);
    public static native void removeAllSounds();
    public static native boolean removeSound(SoundEntry props);
    public static native boolean removeSound(SoundEntry props, String basePath);
    public static native boolean removeSound(LoadItem props);
    public static native boolean removeSound(LoadItem props, String basePath);
    public static native boolean removeSound(String src);
    public static native boolean removeSound(String src, String basePath);
    public static native boolean setMute(boolean value);
    public static native boolean setVolume(double value);
    public static native void stop();

    // Static variants of the EventDispatcher methods
    public static native void initialize(Object target);
    public static native Callback1<Event> addEventListener(String type, Callback1<Event> listener);
    public static native Callback1<Event> addEventListener(String type, Callback1<Event> listener, boolean useCapture);
    public static native boolean dispatchEvent(Event event);
    public static native boolean hasEventListener(String type);
    public static native void off(String type, Callback1<Event> listener);
    public static native void off(String type, Callback1<Event> listener, boolean useCapture);
    public static native Callback1<Event> on(String type, Callback1<Event> listener);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once, Object data);
    public static native Callback1<Event> on(String type, Callback1<Event> listener, Object scope, boolean once, Object data, boolean useCapture);
    public static native void removeAllEventListeners();
    public static native void removeAllEventListeners(String type);
    public static native void removeEventListener(String type, Callback1<Event> listener);
    public static native void removeEventListener(String type, Callback1<Event> listener, boolean useCapture);
    public static native boolean willTrigger(String type);
}
