package org.stjs.javascript.soundjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class WebAudioPlugin extends AbstractPlugin {
    public Object context; // AudioContext
    //public static Object context; // AudioContext
    public Object dynamicsCompressorNode; // AudioNode
    public Object gainNode; // AudioGainNode
    public WebAudioPlugin() { /* compiled code */ }
    public native boolean addPreloadResults(String src);
    public static native boolean isSupported();
    public native void playEmptySound();
}
