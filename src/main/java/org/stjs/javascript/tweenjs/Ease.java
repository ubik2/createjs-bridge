package org.stjs.javascript.tweenjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.functions.Function1;

@STJSBridge
@Namespace("createjs")
public class Ease {
    public static native Function1<Double, Double> backIn();
    public static native Function1<Double, Double> backInOut();
    public static native Function1<Double, Double> backOut();
    public static native Function1<Double, Double> bounceIn();
    public static native Function1<Double, Double> bounceInOut();
    public static native Function1<Double, Double> bounceOut();
    public static native Function1<Double, Double> circIn();
    public static native Function1<Double, Double> circInOut();
    public static native Function1<Double, Double> circOut();
    public static native Function1<Double, Double> cubicIn();
    public static native Function1<Double, Double> cubicInOut();
    public static native Function1<Double, Double> cubicOut();
    public static native Function1<Double, Double> elasticIn();
    public static native Function1<Double, Double> elasticInOut();
    public static native Function1<Double, Double> elasticOut();
    public static native Function1<Double, Double> get(double amount);
    public static native Function1<Double, Double> getBackIn(double amount);
    public static native Function1<Double, Double> getBackInOut(double amount);
    public static native Function1<Double, Double> getBackOut(double amount);
    public static native Function1<Double, Double> getElasticIn(double amplitude, double period);
    public static native Function1<Double, Double> getElasticInOut(double amplitude, double period);
    public static native Function1<Double, Double> getElasticOut(double amplitude, double period);
    public static native Function1<Double, Double> getPowIn(double pow);
    public static native Function1<Double, Double> getPowInOut(double pow);
    public static native Function1<Double, Double> getPowOut(double pow);
    public static native Function1<Double, Double> linear();
    public static native Function1<Double, Double> none();
    public static native Function1<Double, Double> quadIn();
    public static native Function1<Double, Double> quadInOut();
    public static native Function1<Double, Double> quadOut();
    public static native Function1<Double, Double> quartIn();
    public static native Function1<Double, Double> quartInOut();
    public static native Function1<Double, Double> quartOut();
    public static native Function1<Double, Double> quintIn();
    public static native Function1<Double, Double> quintInOut();
    public static native Function1<Double, Double> quintOut();
    public static native Function1<Double, Double> sineIn();
    public static native Function1<Double, Double> sineInOut();
    public static native Function1<Double, Double> sineOut();
}
