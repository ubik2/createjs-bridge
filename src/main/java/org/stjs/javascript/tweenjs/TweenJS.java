package org.stjs.javascript.tweenjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class TweenJS {
    public static String buildDate;
    public static String version;
}
