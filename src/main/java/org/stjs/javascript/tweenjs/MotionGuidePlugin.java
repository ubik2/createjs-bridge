package org.stjs.javascript.tweenjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class MotionGuidePlugin {
    public MotionGuidePlugin() { /* compiled code */ }
    public static native void install();
}
