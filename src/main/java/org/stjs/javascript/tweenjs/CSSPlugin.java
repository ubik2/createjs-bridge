package org.stjs.javascript.tweenjs;

import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class CSSPlugin {
    public static Map<String, String> cssSuffixMap;
    public CSSPlugin() { /* compiled code */ }
    public static native void install();
}
