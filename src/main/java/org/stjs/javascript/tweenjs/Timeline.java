package org.stjs.javascript.tweenjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.annotation.Template;
import org.stjs.javascript.createjs.EventDispatcher;

@STJSBridge
@Namespace("createjs")
public class Timeline extends EventDispatcher {
    @SyntheticType
    public class LabelEntry {
        public String label;
        public long position;
    }
    @Template("toProperty")
    public native long duration();
    public boolean ignoreGlobalPause;
    public boolean loop;
    @Template("toProperty")
    public native long position();
    public Timeline(Array<Tween> tweens, Map<String, Long> labels, Map<String, Object> props) { /* compiled code */ }
    public native void addLabel(String label, long position);
    public native Tween addTween(Tween tween);
    public native String getCurrentLabel();
    public native Array<LabelEntry> getLabels();
    public native void gotoAndPlay(long position);
    public native void gotoAndPlay(String label);
    public native void gotoAndStop(long position);
    public native void gotoAndStop(String label);
    public native boolean removeTween(Tween tween);
    public native long resolve(long position);
    public native long resolve(String label);
    public native void setLabels(Map<String, Long> o);
    public native void setPaused(boolean value);
    public native boolean setPosition(long value);
    public native boolean setPosition(long value, int actionsMode);
    public native void tick(long delta);
    public native void updateDuration();
}
