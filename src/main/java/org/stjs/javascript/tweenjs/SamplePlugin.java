package org.stjs.javascript.tweenjs;

import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class SamplePlugin {
    public static double priority;
    public SamplePlugin() { /* compiled code */ }
    public static native Object init(Tween tween, String prop, Object value);
    public static native Object init(Tween tween, String prop, Object startValue, Map<String, Object> injectProps, Object endValue);
    public static native void install();
    public static native Object tween(Tween tween, String prop, Object value, Map<String, Object> startValues, Map<String, Object> endValues, double ratio, boolean wait, boolean end);
}
