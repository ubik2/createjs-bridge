package org.stjs.javascript.tweenjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.Template;
import org.stjs.javascript.createjs.EventDispatcher;
import org.stjs.javascript.functions.Callback1;
import org.stjs.javascript.functions.Callback2;
import org.stjs.javascript.functions.Callback3;
import org.stjs.javascript.functions.Callback4;
import org.stjs.javascript.functions.Function1;

@STJSBridge
@Namespace("createjs")
public class Tween extends EventDispatcher {
    @Template("toProperty")
    public native long duration();
    public static Object IGNORE;
    public boolean ignoreGlobalPause;
    public static int LOOP;
    public boolean loop;
    public static int NONE;
    @Template("toProperty")
    public native boolean passive();
    public Object pluginData;
    @Template("toProperty")
    public native long position();
    public static int REVERSE;
    @Template("toProperty")
    public native Object target();
    public Tween(Object target) { /* compiled code */ }
    public Tween(Object target, Map<String, Object> props) { /* compiled code */ }
    public Tween(Object target, Map<String, Object> props, Object pluginData) { /* compiled code */ }
    public native Tween call(Callback1<Tween> callback);
    public native Tween call(Callback1<Object> callback, Array<Object> params);
    public native Tween call(Callback1<Object> callback, Array<Object> params, Object scope);
    public native Tween call(Callback2<Object, Object> callback, Array<Object> params);
    public native Tween call(Callback2<Object, Object> callback, Array<Object> params, Object scope);
    public native Tween call(Callback3<Object, Object, Object> callback, Array<Object> params);
    public native Tween call(Callback3<Object, Object, Object> callback, Array<Object> params, Object scope);
    public native Tween call(Callback4<Object, Object, Object, Object> callback, Array<Object> params);
    public native Tween call(Callback4<Object, Object, Object, Object> callback, Array<Object> params, Object scope);
    public static native Tween get(Object target);
    public static native Tween get(Object target, Map<String, Object> props);
    public static native Tween get(Object target, Map<String, Object> props, Object pluginData);
    public static native Tween get(Object target, Map<String, Object> props, Object pluginData, boolean override);
    public static native boolean hasActiveTweens();
    public static native boolean hasActiveTweens(Object target);
    public static native void installPlugin(Object plugin, Array<Object> properties);
    public native Tween pause(Tween tween);
    public native Tween play(Tween tween);
    public static native void removeAllTweens();
    public static native void removeTweens(Object target);
    public native Tween set(Map<String, Object> props);
    public native Tween set(Map<String, Object> props, Object target);
    public native Tween setPaused(boolean value);
    public native Tween setPosition(long value);
    public native Tween setPosition(long value, int actionsMode);
    public native static void tick(long delta, boolean paused);
    public native void tick(long delta);
    public native Tween to(Map<String, Object> props);
    public native Tween to(Map<String, Object> props, long duration);
    public native Tween to(Map<String, Object> props, long duration, Function1<Double, Double> ease);
    public native Tween wait(long duration, boolean passive);
}
