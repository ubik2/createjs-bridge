package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.createjs.Event;

/**
 * Created by robin on 1/19/15.
 */
@STJSBridge
@Namespace("createjs")
public class ProgressEvent extends Event {
    public double loaded;
    public double total;
    public double progress;

    public ProgressEvent(double loaded, double total) {
        super("progress");
        /* compiled code */
    }
    public native ProgressEvent clone();
}
