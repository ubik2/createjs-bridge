package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class AbstractMediaLoader extends AbstractLoader {
    public AbstractMediaLoader(Object loadItem, boolean preferXHR, String type) {
        super(loadItem, preferXHR, type);
        /* compiled code */
    } // loadItem is LoadItem or Object
}
