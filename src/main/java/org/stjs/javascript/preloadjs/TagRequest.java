package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Element;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class TagRequest extends AbstractRequest {
    public TagRequest(LoadItem loadItem, Element tag, String src) {
        super(loadItem);
        /* compiled code */
    }
}
