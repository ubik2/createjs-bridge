package org.stjs.javascript.preloadjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.annotation.Template;
import org.stjs.javascript.dom.Audio;
import org.stjs.javascript.functions.Callback;

@STJSBridge
@Namespace("createjs")
public class LoadQueue extends AbstractLoader {
    public boolean maintainScriptOrder;
    public LoadQueue next;
    public boolean stopOnError;
    public LoadQueue() { /* compiled code */ }
    public LoadQueue(boolean useXHR) { /* compiled code */ }
    public LoadQueue(boolean useXHR, String basePath) { /* compiled code */ }
    public LoadQueue(boolean useXHR, String basePath, String crossOrigin) { /* compiled code */ }
    public LoadQueue(boolean useXHR, String basePath, boolean crossOrigin) { /* compiled code */ }
    public native void close();
    public native LoadItem getItem(String value);
    public native Array<LoadItem> getItems(boolean loaded);
    public native Object getResult(String value);
    public native Object getResult(String value, boolean rawResult);
    public native void installPlugin(Class plugin);
    public native void loadFile(String file);
    public native void loadFile(String file, boolean loadNow);
    public native void loadFile(String file, boolean loadNow, String basePath);
    public native void loadFile(LoadItem file);
    public native void loadFile(LoadItem file, boolean loadNow);
    public native void loadFile(LoadItem file, boolean loadNow, String basePath);
    // I don't think these are valid anymore
//  public native void loadFile(Audio file);
//  public native void loadFile(Audio file, boolean loadNow);
//  public native void loadFile(Audio file, boolean loadNow, String basePath);
    public native void loadManifest(String manifest);
    public native void loadManifest(String manifest, boolean loadNow);
    public native void loadManifest(String manifest, boolean loadNow, String basePath);
    public native void loadManifest(Map<String, Object> manifest);
    public native void loadManifest(Map<String, Object> manifest, boolean loadNow);
    public native void loadManifest(Map<String, Object> manifest, boolean loadNow, String basePath);
    public native void loadManifest(Array<Object> manifest); // Object should be String or LoadItem
    public native void loadManifest(Array<Object> manifest, boolean loadNow);
    public native void loadManifest(Array<Object> manifest, boolean loadNow, String basePath);
    public native void registerLoader(Class loader);
    public native void remove(String idOrUrl);
    public native void remove(String idOrUrl1, String idOrUrl2);
    public native void remove(String idOrUrl1, String idOrUrl2, String idOrUrl3);
    public native void remove(String idOrUrl1, String idOrUrl2, String idOrUrl3, String idOrUrl4);
    public native void remove(Array<String> idsOrUrls);
    public native void removeAll();
    public native void reset();
    public native void setMaxConnections(int value);
    public native void setPaused(boolean value);
    public native boolean setPreferXHR(boolean value);
    public native void unregisterLoader(Class loader);
}
