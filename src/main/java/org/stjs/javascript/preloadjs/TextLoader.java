package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class TextLoader extends AbstractLoader {
    public TextLoader(Object loadItem) { /* compiled code */ } // loadItem is Object or LoadItem
    public static native boolean canLoadItem(Object loadItem); // loadItem is Object or LoadItem
}
