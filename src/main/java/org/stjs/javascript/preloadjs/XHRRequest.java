package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class XHRRequest extends AbstractLoader {
    public XHRRequest(Object loadItem) { /* compiled code */ }
    public XHRRequest(Object loadItem, boolean preferXHR) { /* compiled code */ }
    public XHRRequest(Object loadItem, boolean preferXHR, String type) { /* compiled code */ }
    public native String getAllResponseHeaders();
    public native String getResponseHeader(String header);
}
