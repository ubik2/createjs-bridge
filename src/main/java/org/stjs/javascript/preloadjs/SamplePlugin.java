package org.stjs.javascript.preloadjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.createjs.Event;
import org.stjs.javascript.functions.Function2;

@STJSBridge
@Namespace("createjs")
public class SamplePlugin {
    @SyntheticType
    public static class PreloadHandler {
        public Function2<AbstractLoader, LoadItem, LoadQueue> callback;
        public Array<String> types;
        public Array<String> extensions;
    }
    public native void fileLoadHandler(Event event);
    public native PreloadHandler getPreloadHandlers();
    public native AbstractLoader preloadHandler(Object loadItem, LoadQueue queue); // loadItem is Object or LoadItem
}
