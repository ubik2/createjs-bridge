package org.stjs.javascript.preloadjs;

import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class LoadItem {
    public String callback;
    public boolean crossOrigin;
    public Object data;
    public Map<String, String> headers;
    public String id;
    public long loadTimeout;
    public boolean maintainOrder;
    public String method;
    public String mimeType;
    public String src;
    public String type;
    public Map<String, String> values;
    public boolean withCredentials;

    public LoadItem() { /* compiled code */ }
    public static native Object create(Object value);
    public native LoadItem set(Map<String, Object> props);
}
