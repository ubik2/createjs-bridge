package org.stjs.javascript.preloadjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.RegExp;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;

/**
 * Created by robin on 1/19/15.
 */
@STJSBridge
@Namespace("createjs")
public class RequestUtils {
    @SyntheticType
    public static class PathInfo {
        public boolean absolute;
        public String extension;
        public boolean relative;
    }

    public static RegExp ABSOLUTE_PATT;
    public static RegExp EXTENSION_PATT;
    public static RegExp RELATIVE_PATH;

    public static native String buildPath(String src);
    public static native String buildPath(String src, Object data);
    public static native String formatQueryString(Object data);
    public static native String formatQueryString(Object data, Array<String> query);
    public static native String getTypeByExtension(String extension);
    public static native boolean isAudioTag(Object item);
    public static native boolean isBinary(String type);
    public static native boolean isCrossDomain(Object loadItem); // loadItem is LoadItem or Object
    public static native boolean isImageTag(Object item);
    public static native boolean isLocal(Object loadItem); // loadItem is LoadItem or Object
    public static native boolean isText(String type);
    public static native boolean isVideoTag(Object item);
    public static native Object parseURI(String path);
}
