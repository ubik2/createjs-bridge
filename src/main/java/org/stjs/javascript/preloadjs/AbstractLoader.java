package org.stjs.javascript.preloadjs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.createjs.EventDispatcher;
import org.stjs.javascript.functions.Function1;

@STJSBridge
@Namespace("createjs")
public class AbstractLoader extends EventDispatcher {
    public static String BINARY;
    public boolean canceled;
    public static String CSS;
    public static String GET;
    public static String IMAGE;
    public static String JAVASCRIPT;
    public static String JSON;
    public static String JSONP;
    public boolean loaded;
    public static String MANIFEST;
    public static String POST;
    public double progress;
    public Function1<Object, Object> resultFormatter;
    public static String SOUND;
    public static String SPRITESHEET;
    public static String SVG;
    public static String TEXT;
    public String type;
    public static String VIDEO;
    public static String XML;

    public AbstractLoader() { /* compiled code */ }
    public AbstractLoader(Object loadItem) { /* compiled code */ }
    public AbstractLoader(Object loadItem, boolean preferXHR) { /* compiled code */ }
    public AbstractLoader(Object loadItem, boolean preferXHR, String type) { /* compiled code */ }
    public native void cancel();
    public native void destroy();
    public native LoadItem getItem();
    public native Array<LoadItem> getLoadedItems();
    public native Object getResult();
    public native Object getResult(boolean raw);
    public native Object getTag();
    public native void load();
    public native void setTag(Object tag);
}
