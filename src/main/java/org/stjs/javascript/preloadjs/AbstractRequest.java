package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

/**
 * Created by robin on 1/18/15.
 */
@STJSBridge
@Namespace("createjs")
public class AbstractRequest {
    public AbstractRequest(LoadItem item) { /* compiled code */ }
    public native void cancel();
    public native void destroy();
    public native void load();
}
