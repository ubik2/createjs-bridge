package org.stjs.javascript.preloadjs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Audio;
import org.stjs.javascript.dom.Video;

/**
 * Created by robin on 1/19/15.
 */
@STJSBridge
@Namespace("createjs")
public class MediaTagRequest extends TagRequest {
    public MediaTagRequest(LoadItem loadItem, Audio tag, String srcAttribute) {
        super(loadItem, tag, srcAttribute);
        /* compiled code */
    }
    public MediaTagRequest(LoadItem loadItem, Video tag, String srcAttribute) {
        super(loadItem, tag, srcAttribute);
        /* compiled code */
    }
}
