package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class DisplayProps {
    public double alpha;
    public String compositeOperation;
    public Matrix2D matrix;
    public Shadow shadow;
    public boolean visible;
    public DisplayProps() { /* compiled code */ }
    public DisplayProps(boolean visible) { /* compiled code */ }
    public DisplayProps(boolean visible, double alpha) { /* compiled code */ }
    public DisplayProps(boolean visible, double alpha, Shadow shadow) { /* compiled code */ }
    public DisplayProps(boolean visible, double alpha, Shadow shadow, String compositeOperation) { /* compiled code */ }
    public DisplayProps(boolean visible, double alpha, Shadow shadow, String compositeOperation, Matrix2D matrix) { /* compiled code */ }
    public native DisplayProps append(boolean visible, double alpha, Shadow shadow, String compositeOperation);
    public native DisplayProps append(boolean visible, double alpha, Shadow shadow, String compositeOperation, Matrix2D matrix);
    public native DisplayProps clone();
    public native DisplayProps identity();
    public native DisplayProps prepend(boolean visible, double alpha, Shadow shadow, String compositeOperation);
    public native DisplayProps prepend(boolean visible, double alpha, Shadow shadow, String compositeOperation, Matrix2D matrix);
    public native DisplayProps setValues();
    public native DisplayProps setValues(boolean visible);
    public native DisplayProps setValues(boolean visible, double alpha);
    public native DisplayProps setValues(boolean visible, double alpha, Shadow shadow);
    public native DisplayProps setValues(boolean visible, double alpha, Shadow shadow, String compositeOperation);
    public native DisplayProps setValues(boolean visible, double alpha, Shadow shadow, String compositeOperation, Matrix2D matrix);
}