package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Canvas;
import org.stjs.javascript.dom.Image;

@STJSBridge
@Namespace("createjs")
public class SpriteStage extends Stage {
    public static int INDICES_PER_BOX;
    public boolean isWebGL;
    public static int MAX_INDEX_SIZE;
    public static int NUM_VERTEX_PROPERTIES;
    public static int NUM_VERTEX_PROPERTIES_PER_BOX;
    public static int POINTS_PER_BOX;

    public SpriteStage(Canvas canvas, boolean preserveDrawingBuffer, boolean antialias) { super(canvas); /* compiled code */ }
    public SpriteStage(String canvas, boolean preserveDrawingBuffer, boolean antialias) { super(canvas); /* compiled code */ }
    public native void clearImageTexture(Image image);
    public native void updateViewport(double width, double height);
}
