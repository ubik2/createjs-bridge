package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class BitmapText extends DisplayObject {
    public double letterSpacing;
    public double lineHeight;
    public static int maxPoolSize;
    public double spaceWidth;
    public SpriteSheet spriteSheet;
    public String text;
    public BitmapText() { /* compiled code */ }
    public BitmapText(String text) { /* compiled code */ }
    public BitmapText(SpriteSheet spriteSheet) { /* compiled code */ }
    public BitmapText(String text, SpriteSheet spriteSheet) { /* compiled code */ }
}
