package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Shape extends DisplayObject {
    public Graphics graphics;

    public Shape() { /* compiled code */ }
    public Shape(Graphics graphics) { /* compiled code */ }
    public native Shape clone();
    public native Shape clone(boolean recursive);
    public native boolean draw(Object ctx);
    public native boolean draw(Object ctx, boolean ignoreCache);
}
