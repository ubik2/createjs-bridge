package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Image;

@STJSBridge
@Namespace("createjs")
public class AlphaMaskFilter extends Filter {
    public Image mask;
    public AlphaMaskFilter(Image mask) { /* compiled code */ }
}
