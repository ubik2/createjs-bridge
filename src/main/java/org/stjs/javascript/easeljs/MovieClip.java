package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.Template;
import org.stjs.javascript.tweenjs.Timeline;

@STJSBridge
@Namespace("createjs")
public class MovieClip extends Container {
    public boolean actionsEnabled;
    public boolean autoReset;
    public static String buildDate;
    public int currentFrame;
    @Template("toProperty")
    public native String currentLabel();
    public Array<Rectangle> frameBounds;
    public double framerate;
    public static String INDEPENDENT;
    @Template("toProperty")
    public native Array<Timeline.LabelEntry> labels();
    public boolean loop;
    public String mode;
    public boolean paused;
    public static String SINGLE_FRAME;
    public int startPosition;
    public static String SYNCHED;
    public Timeline timeline;
    public static String version;

    public MovieClip() { /* compiled code */ }
    public MovieClip(String mode) { /* compiled code */ }
    public MovieClip(String mode, long startPosition) { /* compiled code */ }
    public MovieClip(String mode, long startPosition, boolean loop) { /* compiled code */ }
    public MovieClip(String mode, long startPosition, boolean loop, Map<String, Long> labels) { /* compiled code */ }
    public native void advance();
    public native void advance(long time);
    public native MovieClip clone();
    public native void gotoAndPlay(long position);
    public native void gotoAndPlay(String label);
    public native void gotoAndStop(long position);
    public native void gotoAndStop(String label);
    public native void play();
    public native void stop();
}
