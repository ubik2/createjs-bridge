package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class BlurFilter extends Filter {
    public double blurX;
    public double blurY;
    public int quality;
    public BlurFilter() { /* compiled code */ }
    public BlurFilter(double blurX) { /* compiled code */ }
    public BlurFilter(double blurX, double blurY) { /* compiled code */ }
    public BlurFilter(double blurX, double blurY, int quality) { /* compiled code */ }
    public native BlurFilter clone();
}
