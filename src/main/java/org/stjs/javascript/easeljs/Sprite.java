package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.Template;

@STJSBridge
@Namespace("createjs")
public class Sprite extends DisplayObject {
    @Template("toProperty")
    public native String currentAnimation();
    public int currentAnimationFrame;
    public int currentFrame;
    public double framerate;
    public boolean paused;
    public SpriteSheet spriteSheet;

    public Sprite(SpriteSheet spriteSheet) { /* compiled code */ }
    public Sprite(SpriteSheet spriteSheet, int frame) { /* compiled code */ }
    public Sprite(SpriteSheet spriteSheet, String animation) { /* compiled code */ }
    public native void advance();
    public native void advance(long time);
    public native Sprite clone();
    public native void gotoAndPlay(long position);
    public native void gotoAndPlay(String label);
    public native void gotoAndStop(long position);
    public native void gotoAndStop(String label);
    public native void play();
    public native void stop();
}
