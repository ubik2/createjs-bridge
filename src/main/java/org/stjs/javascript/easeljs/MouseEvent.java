package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.createjs.Event;
import org.stjs.javascript.dom.DOMEvent;

@STJSBridge
@Namespace("createjs")
public class MouseEvent extends Event {
    public boolean isTouch;
    public double localX;
    public double localY;
    public DOMEvent nativeEvent;
    public int pointerID;
    public boolean primary;
    public double rawX;
    public double rawY;
    public double stageX;
    public double stageY;

    public MouseEvent(String type, boolean bubbles, boolean cancelable, double stageX, double stageY, DOMEvent nativeEvent, int pointerID, boolean primary, double rawX, double rawY) {
        super(type, bubbles, cancelable);
        /* compiled code */
    }
    public native MouseEvent clone();
}
