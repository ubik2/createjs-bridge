package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.easeljs.Stage;

@STJSBridge
@Namespace("createjs")
public class Touch {
    public static native void disable(Stage stage);
    public static native boolean enable(Stage stage);
    public static native boolean enable(Stage stage, boolean singleTouch);
    public static native boolean enable(Stage stage, boolean singleTouch, boolean allowDefault);
    public static native boolean isSupported();
}
