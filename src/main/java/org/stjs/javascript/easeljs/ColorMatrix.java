package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class ColorMatrix {
    public double brightness;
    public double contrast;
    public double saturaton;
    public double hue;
    public ColorMatrix(double brightness, double contrast, double saturation, double hue) { /* compiled code */ }
    public native ColorMatrix adjustBrightness(double value);
    public native ColorMatrix adjustColor(double brightness, double contrast, double saturation, double hue);
    public native ColorMatrix adjustContrast(double value);
    public native ColorMatrix adjustHue(double value);
    public native ColorMatrix adjustSaturation(double value);
    public native ColorMatrix clone();
    public native ColorMatrix concat(Array<Double> matrix);
    public native ColorMatrix copy(Array<Double> matrix);
    public native ColorMatrix reset();
    public native ColorMatrix setColor(double brightness, double contrast, double saturation, double hue);
    public native Array<Double> toArray();
}
