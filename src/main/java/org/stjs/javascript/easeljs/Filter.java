package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Filter {
    public Filter() { /* compiled code */ }
    public native boolean _applyFilter(Object imageData);
    public native boolean applyFilter(Object ctx, double x, double y);
    public native boolean applyFilter(Object ctx, double x, double y, Object targetCtx);
    public native boolean applyFilter(Object ctx, double x, double y, Object targetCtx, double targetX);
    public native boolean applyFilter(Object ctx, double x, double y, Object targetCtx, double targetX, double targetY);
    public native Filter clone();
    public native Rectangle getBounds();
    public native Rectangle getBounds(Rectangle rect);
}
