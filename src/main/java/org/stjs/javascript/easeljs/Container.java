package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.Template;
import org.stjs.javascript.functions.Function2;

@STJSBridge
@Namespace("createjs")
public class Container extends DisplayObject {
    public Array<DisplayObject> children;
    public boolean mouseChildren;
    @Template("toProperty")
    public native int numChidren();
    public boolean tickChildren;
    public Container() { /* compiled code */ }
    public native DisplayObject addChild(DisplayObject child);
    public native DisplayObject addChildAt(DisplayObject child, int index);
    public native Container clone();
    public native Container clone(boolean recursive);
    public native boolean contains(DisplayObject child);
    public native DisplayObject getChildAt(int index);
    public native DisplayObject getChildByName(String name);
    public native int getChildIndex(DisplayObject child);
    public native Array<DisplayObject> getObjectsUnderPoint(double x, double y);
    public native Array<DisplayObject> getObjectsUnderPoint(double x, double y, int mode);
    public native DisplayObject getObjectUnderPoint(double x, double y);
    public native DisplayObject getObjectUnderPoint(double x, double y, int mode);
    public native void removeAllChildren();
    public native boolean removeChild(DisplayObject child);
    public native boolean removeChildAt(int index);
    public native void setChildIndex(DisplayObject child, int index);
    public native void sortChildren(Function2<DisplayObject, DisplayObject, Integer> sortFunction);
    public native void swapChildren(DisplayObject child1, DisplayObject child2);
    public native void swapChildrenAt(int index1, int index2);
}
