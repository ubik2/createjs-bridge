package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Point {
    public double x;
    public double y;

    public Point() { /* compiled code */ }
    public Point(double x) { /* compiled code */ }
    public Point(double x, double y) { /* compiled code */ }
    public native Point clone();
    public native Point copy(Point point);
    public native Point setValues();
    public native Point setValues(double x);
    public native Point setValues(double x, double y);
}
