package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Shadow {
    public static Shadow identity;

    public Shadow(String color, double offsetX, double offsetY, double blur) { /* compiled code */ }
    public native Shadow clone();
}
