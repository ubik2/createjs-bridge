package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class ColorFilter extends Filter {
    public double alphaMultiplier;
    public int alphaOffset;
    public double blueMultiplier;
    public int blueOffset;
    public double greenMultiplier;
    public int greenOffset;
    public double redMultiplier;
    public int redOffset;
    public ColorFilter() { /* compiled code */ }
    public ColorFilter(double redMultiplier) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier, double blueMultiplier) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier, double blueMultiplier, double alphaMultiplier) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier, double blueMultiplier, double alphaMultiplier, int redOffset) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier, double blueMultiplier, double alphaMultiplier, int redOffset, int greenOffset) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier, double blueMultiplier, double alphaMultiplier, int redOffset, int greenOffset, int blueOffset) { /* compiled code */ }
    public ColorFilter(double redMultiplier, double greenMultiplier, double blueMultiplier, double alphaMultiplier, int redOffset, int greenOffset, int blueOffset, int alphaOffset) { /* compiled code */ }
    public native ColorFilter clone();
}
