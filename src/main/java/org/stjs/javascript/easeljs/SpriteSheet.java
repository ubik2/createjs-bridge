package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;
import org.stjs.javascript.annotation.Template;
import org.stjs.javascript.createjs.EventDispatcher;
import org.stjs.javascript.dom.Image;

@STJSBridge
@Namespace("createjs")
public class SpriteSheet extends EventDispatcher {
    @SyntheticType
    public static class AnimationEntry {
        public Array<Integer> frames;
        public double speed;
        public String name;
        public String next;
    }
    @SyntheticType
    public static class FrameEntry {
        public Image image;
        public Rectangle rect;
        public double regX;
        public double regY;
    }
    @Template("toProperty")
    public native Array<String> animations();
    public boolean complete;
    public double framerate;

    public SpriteSheet(Map<String, Object> data) { /* compiled code */ }
    public native SpriteSheet clone();
    public native AnimationEntry getAnimation(String name);
    public native FrameEntry getFrame(int frameIndex);
    public native Rectangle getFrameBounds(int frameIndex);
    public native Rectangle getFrameBounds(int frameIndex, Rectangle rectangle);
    public native int getNumFrames(String animation);
}
