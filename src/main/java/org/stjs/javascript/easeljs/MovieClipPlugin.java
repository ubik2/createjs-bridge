package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.tweenjs.Tween;

@STJSBridge
@Namespace("createjs")
public class MovieClipPlugin {
    public static native void tween(Tween tween, String prop, Object value, Array startValues, Array endValues, double ratio, Object wait, Object end);
}