package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.SyntheticType;

@STJSBridge
@Namespace("createjs")
public class Text extends DisplayObject {
    @SyntheticType
    public static class Metrics {
        public double height;
        public double lineHeight;
        public Array<String> lines;
        public double vOffset;
        public double width;
    }
    public String color;
    public String font;
    public double lineHeight;
    public double lineWidth;
    public double maxWidth;
    public double outline;
    public String text;
    public String textAlign;
    public String textBaseline;

    public Text() { /* compiled code */ }
    public Text(String text) { /* compiled code */ }
    public Text(String text, String font) { /* compiled code */ }
    public Text(String text, String font, String color) { /* compiled code */ }
    public native Text clone();
    public native double getMeasuredHeight();
    public native double getMeasuredLineHeight();
    public native double getMeasuredWidth();
    public native Metrics getMetrics();
}
