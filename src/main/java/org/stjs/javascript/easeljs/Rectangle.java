package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Rectangle {
    public double height;
    public double width;
    public double x;
    public double y;

    public Rectangle() { /* compiled code */ }
    public Rectangle(double x) { /* compiled code */ }
    public Rectangle(double x, double y) { /* compiled code */ }
    public Rectangle(double x, double y, double width) { /* compiled code */ }
    public Rectangle(double x, double y, double width, double height) { /* compiled code */ }
    public native Rectangle clone();
    public native boolean contains(double x, double y);
    public native boolean contains(double x, double y, double width);
    public native boolean contains(double x, double y, double width, double height);
    public native Rectangle copy(Rectangle rectangle);
    public native Rectangle extend(double x, double y);
    public native Rectangle extend(double x, double y, double width);
    public native Rectangle extend(double x, double y, double width, double height);
    public native Rectangle pad();
    public native Rectangle pad(double top);
    public native Rectangle pad(double top, double left);
    public native Rectangle pad(double top, double left, double right);
    public native Rectangle pad(double top, double left, double right, double bottom);
    public native Rectangle intersection(Rectangle rect);
    public native boolean intersects(Rectangle rect);
    public native boolean isEmpty();
    public native Rectangle setValues();
    public native Rectangle setValues(double x);
    public native Rectangle setValues(double x, double y);
    public native Rectangle setValues(double x, double y, double width);
    public native Rectangle setValues(double x, double y, double width, double height);
    public native Rectangle union(Rectangle rect);
}
