package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.createjs.Event;
import org.stjs.javascript.dom.Canvas;
import org.stjs.javascript.functions.Callback1;

@STJSBridge
@Namespace("createjs")
public class Stage extends Container {
    public boolean autoClear;
    public Canvas canvas;
    public Callback1<Event> handleEvent;
    public boolean mouseInBounds;
    public boolean mouseMoveOutside;
    public double mouseX;
    public double mouseY;
    public Stage nextStage;
    public boolean tickOnUpdate;

    public Stage(Canvas canvas) { /* compiled code */ }
    public Stage(String canvas) { /* compiled code */ }
    public native void clear();
    public native void enableDOMEvents();
    public native void enableDOMEvents(boolean enable);
    public native void enableMouseOver();
    public native void enableMouseOver(double frequency);
    public native void tick(Object props);
    public native String toDataURL();
    public native String toDataURL(String backgroundColor);
    public native String toDataURL(String backgroundColor, String mimeType);
    public native void update();
    public native void update(Object params);
}
