package org.stjs.javascript.easeljs;


import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Canvas;
import org.stjs.javascript.dom.Element;
import org.stjs.javascript.dom.Image;
import org.stjs.javascript.dom.Video;

@STJSBridge
@Namespace("createjs")
public class Bitmap extends DisplayObject {
    public Element image;
    public Rectangle sourceRect;
    public Bitmap(Image image) { /* compiled code */ }
    public Bitmap(Canvas image) { /* compiled code */ }
    public Bitmap(Video image) { /* compiled code */ }
    public Bitmap(String imageUri) { /* compiled code */ }
    public native Bitmap clone();
}
