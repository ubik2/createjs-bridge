package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class AlphaMapFilter extends Filter {
    public Object alphaMap; // Image or HTMLCanvas
    public AlphaMapFilter(Object alphaMap) { /* compiled code */ }
}
