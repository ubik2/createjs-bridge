package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class ColorMatrixFilter extends Filter {
    public Object matrix;
    public ColorMatrixFilter(Array<Double> matrix) { /* compiled code */ }
    public ColorMatrixFilter(ColorMatrix matrix) { /* compiled code */ }
}
