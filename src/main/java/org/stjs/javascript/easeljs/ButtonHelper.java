package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.Template;

@STJSBridge
@Namespace("createjs")
public class ButtonHelper {
    @Template("toProperty")
    public native Object downLabel();
    @Template("toProperty")
    public native void downLabel(String value);
    @Template("toProperty")
    public native void downLabel(int value);
    public boolean enabled;
    @Template("toProperty")
    public native Object outLabel();
    @Template("toProperty")
    public native void outLabel(String value);
    @Template("toProperty")
    public native void outLabel(int value);
    @Template("toProperty")
    public native Object overLabel();
    @Template("toProperty")
    public native void overLabel(String value);
    @Template("toProperty")
    public native void overLabel(int value);
    public boolean play;
    @Template("toProperty")
    public native DisplayObject target();
    public ButtonHelper(Sprite target) { /* compiled code */ }
    public ButtonHelper(Sprite target, String outLabel) { /* compiled code */ }
    public ButtonHelper(Sprite target, String outLabel, String overLabel) { /* compiled code */ }
    public ButtonHelper(Sprite target, String outLabel, String overLabel, String downLabel) { /* compiled code */ }
    public ButtonHelper(Sprite target, String outLabel, String overLabel, String downLabel, boolean play) { /* compiled code */ }
    public ButtonHelper(Sprite target, String outLabel, String overLabel, String downLabel, boolean play, DisplayObject hitArea) { /* compiled code */ }
    public ButtonHelper(Sprite target, String outLabel, String overLabel, String downLabel, boolean play, DisplayObject hitArea, String hitLabel) { /* compiled code */ }
    public ButtonHelper(MovieClip target) { /* compiled code */ }
    public ButtonHelper(MovieClip target, String outLabel) { /* compiled code */ }
    public ButtonHelper(MovieClip target, String outLabel, String overLabel) { /* compiled code */ }
    public ButtonHelper(MovieClip target, String outLabel, String overLabel, String downLabel) { /* compiled code */ }
    public ButtonHelper(MovieClip target, String outLabel, String overLabel, String downLabel, boolean play) { /* compiled code */ }
    public ButtonHelper(MovieClip target, String outLabel, String overLabel, String downLabel, boolean play, DisplayObject hitArea) { /* compiled code */ }
    public ButtonHelper(MovieClip target, String outLabel, String overLabel, String downLabel, boolean play, DisplayObject hitArea, String hitLabel) { /* compiled code */ }
}
