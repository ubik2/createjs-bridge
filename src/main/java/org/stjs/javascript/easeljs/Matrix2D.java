package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class Matrix2D {
    public double a;
    public double b;
    public double c;
    public double d;
    public static double DEG_TO_RAD;
    public static Matrix2D identity;
    public double tx;
    public double ty;

    public Matrix2D() { /* compiled code */ }
    public Matrix2D(double a) { /* compiled code */ }
    public Matrix2D(double a, double b) { /* compiled code */ }
    public Matrix2D(double a, double b, double c) { /* compiled code */ }
    public Matrix2D(double a, double b, double c, double d) { /* compiled code */ }
    public Matrix2D(double a, double b, double c, double d, double tx) { /* compiled code */ }
    public Matrix2D(double a, double b, double c, double d, double tx, double ty) { /* compiled code */ }
    public native Matrix2D append(double a, double b, double c, double d, double tx, double ty);
    public native Matrix2D appendMatrix(Matrix2D matrix);
    public native Matrix2D appendTransform(double x, double y, double scaleX, double scaleY, double rotation, double skewX, double skewY);
    public native Matrix2D appendTransform(double x, double y, double scaleX, double scaleY, double rotation, double skewX, double skewY, double regX);
    public native Matrix2D appendTransform(double x, double y, double scaleX, double scaleY, double rotation, double skewX, double skewY, double regX, double regY);
    public native Matrix2D clone();
    public native Matrix2D copy(Matrix2D matrix);
    public native Matrix2D decompose(Object target);
    public native Matrix2D identity();
    public native Matrix2D invert();
    public native boolean isIdentity();
    public native Matrix2D prepend(double a, double b, double c, double d, double tx, double ty);
    public native Matrix2D prependMatrix(Matrix2D matrix);
    public native Matrix2D prependTransform(double x, double y, double scaleX, double scaleY, double rotation, double skewX, double skewY);
    public native Matrix2D prependTransform(double x, double y, double scaleX, double scaleY, double rotation, double skewX, double skewY, double regX);
    public native Matrix2D prependTransform(double x, double y, double scaleX, double scaleY, double rotation, double skewX, double skewY, double regX, double regY);
    public native Matrix2D rotate(double angle);
    public native Matrix2D scale(double x, double y);
    public native Matrix2D setValues();
    public native Matrix2D setValues(double a);
    public native Matrix2D setValues(double a, double b);
    public native Matrix2D setValues(double a, double b, double c);
    public native Matrix2D setValues(double a, double b, double c, double d);
    public native Matrix2D setValues(double a, double b, double c, double d, double tx);
    public native Matrix2D setValues(double a, double b, double c, double d, double tx, double ty);
    public native Matrix2D skew(double skewX, double skewY);
    public native Point transformPoint(double x, double y);
    public native Point transformPoint(double x, double y, Point pt);
    public native Matrix2D translate(double x, double y);
}
