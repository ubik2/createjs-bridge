package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.Map;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Canvas;
import org.stjs.javascript.dom.Image;
import org.stjs.javascript.dom.Video;

@STJSBridge
@Namespace("createjs")
public class Graphics {
    public static Map<String, Integer> BASE_64;
    public static Graphics.BeginPath beginCmd;
    public Object command;
    public Array<Object> instructions;
    public static Array<String> STROKE_CAPS_MAP;
    public static Array<String> STROKE_JOINTS_MAP;

    public Graphics() { /* compiled code */ }
    public native Graphics append(Object command, boolean clean);
    public native Graphics arc(double x, double y, double radius, double startAngle, double endAngle, boolean anticlockwise);
    public native Graphics arcTo(double x1, double y1, double x2, double y2, double radius);
    public native Graphics beginBitmapFill(Image image);
    public native Graphics beginBitmapFill(Image image, String repetition);
    public native Graphics beginBitmapFill(Image image, String repetition, Matrix2D matrix);
    public native Graphics beginBitmapFill(Canvas image);
    public native Graphics beginBitmapFill(Canvas image, String repetition);
    public native Graphics beginBitmapFill(Canvas image, String repetition, Matrix2D matrix);
    public native Graphics beginBitmapFill(Video image);
    public native Graphics beginBitmapFill(Video image, String repetition);
    public native Graphics beginBitmapFill(Video image, String repetition, Matrix2D matrix);
    public native Graphics beginBitmapStroke(Image image);
    public native Graphics beginBitmapStroke(Image image, String repetition);
    public native Graphics beginBitmapStroke(Canvas image);
    public native Graphics beginBitmapStroke(Canvas image, String repetition);
    public native Graphics beginBitmapStroke(Video image);
    public native Graphics beginBitmapStroke(Video image, String repetition);
    public native Graphics beginFill(String color);
    public native Graphics beginLinearGradientFill(Array<String> colors, Array<Double> ratios, double x0, double y0, double x1, double y1);
    public native Graphics beginLinearGradientStroke(Array<String> colors, Array<Double> ratios, double x0, double y0, double x1, double y1);
    public native Graphics beginRadialGradientFill(Array<String> colors, Array<Double> ratios, double x0, double y0, double r0, double x1, double y1, double r1);
    public native Graphics beginRadialGradientStroke(Array<String> colors, Array<Double> ratios, double x0, double y0, double r0, double x1, double y1, double r1);
    public native Graphics beginStroke(String color);
    public native Graphics bezierCurveTo(double cp1x, double cp1y, double cp2x, double cp2y, double x, double y);
    public native Graphics clear();
    public native Graphics clone();
    public native Graphics closePath();
    public native Graphics decodePath(String str);
    public native void draw(Object ctx);
    public native void drawAsPath(Object ctx);
    public native void drawAsPath(Object ctx, Object data);
    public native Graphics drawCircle(double x, double y, double radius);
    public native Graphics drawEllipse(double x, double y, double w, double h);
    public native Graphics drawPolyStar(double x, double y, double radius, int sides, double pointSize, double angle);
    public native Graphics drawRect(double x, double y, double w, double h);
    public native Graphics drawRoundRect(double x, double y, double w, double h, double radius);
    public native Graphics drawRoundRectComplex(double x, double y, double w, double h, double radiusTL, double radiusTR, double radiusBR, double radiusBL);
    public native Graphics endFill();
    public native Graphics endStroke();
    public native static String getHSL(int hue, int saturation, int lightness);
    public native static String getHSL(int hue, int saturation, int lightness, double alpha);
    public native static String getRGB(int r, int g, int b);
    public native static String getRGB(int r, int g, int b, double alpha);
    public native boolean isEmpty();
    public native Graphics lineTo(double x, double y);
    public native Graphics moveTo(double x, double y);
    public native Graphics quadraticCurveTo(double cpx, double cpy, double x, double y);
    public native Graphics rect(double x, double y, double w, double h);
    public native Graphics setStrokeStyle(double thickness);
    public native Graphics setStrokeStyle(double thickness, String caps);
    public native Graphics setStrokeStyle(double thickness, String caps, String joints);
    public native Graphics setStrokeStyle(double thickness, String caps, String joints, double miterLimit);
    public native Graphics setStrokeStyle(double thickness, String caps, String joints, double miterLimit, boolean ignoreScale);
    public native Graphics store();
    public native Graphics unstore();

    public static class Arc {
        public boolean anticlockwise;
        public double endAngle;
        public double radius;
        public double startAngle;
        public double x;
        public double y;

        public Arc(double x, double y, double radius, double startAngle, double endAngle, boolean anticlockwise) { /* compiled code */ }
    }

    public static class ArcTo {
        public double radius;
        public double x1;
        public double x2;
        public double y1;
        public double y2;

        public ArcTo(double x1, double y1, double x2, double y2, double redius) { /* compiled code */ }
    }

    public static class BeginPath {
        public BeginPath() { /* compiled code */ }
    }

    public static class BezierCurveTo {
        public double cp1x;
        public double cp1y;
        public double cp2x;
        public double cp2y;
        public double x;
        public double y;

        public BezierCurveTo(double cp1x, double cp1y, double cp2x, double cp2y, double x, double y) { /* compiled code */ }
    }

    public static class Circle {
        public double radius;
        public double x;
        public double y;

        public Circle(double x, double y, double radius) { /* compiled code */ }
    }

    public static class ClosePath {
        public ClosePath() { /* compiled code */ }
    }

    public static class Fill {
        public Matrix2D matrix;
        public Object style;

        public Fill(Object style, Matrix2D matrix) { /* compiled code */ }
        public native Fill bitmap(Image image);
        public native Fill bitmap(Image image, String repetition);
        public native Fill linearGradient(Array<String> colors, Array<Double> ratios, double x0, double y0, double x1, double y1);
        public native Fill radialGradient(Array<String> colors, Array<Double> ratios, double x0, double y0, double r0, double x1, double y1, double r1);
    }

    public static class LineTo {
        public double x;
        public double y;

        public LineTo(double x, double y) { /* compiled code */ }
    }

    public static class MoveTo {
        public double x;
        public double y;

        public MoveTo(double x, double y) { /* compiled code */ }
    }

    public static class PolyStar {
        public double angle;
        public double pointSize;
        public double radius;
        public int sides;
        public double x;
        public double y;

        public PolyStar(double x, double y, double radius, int sides, double pointSize, double angle) { /* compiled code */ }
    }

    public static class QuadraticCurveTo {
        public double cpx;
        public double cpy;
        public double x;
        public double y;

        public QuadraticCurveTo(double cpx, double cpy, double x, double y) { /* compiled code */ }
    }

    public static class Rect {
        public double h;
        public double w;
        public double x;
        public double y;

        public Rect(double x, double y, double w, double h) { /* compiled code */ }
    }

    public static class RoundRect {
        public double h;
        public double radiusBL;
        public double radiusBR;
        public double radiusTL;
        public double radiusTR;
        public double w;
        public double x;
        public double y;

        public RoundRect(double x, double y, double w, double h, double radiusTL, double radiusTR, double radiusBR, double radiusBL) { /* compiled code */ }
    }

    public static class Stroke {
        public boolean ignoreScale;
        public Object style;

        public Stroke(Object style, boolean ignoreScale) { /* compiled code */ }
        public native Stroke bitmap(Image image);
        public native Stroke bitmap(Image image, String repetition);
        public native Stroke linearGradient(Array<String> colors, Array<Double> ratios, double x0, double y0, double x1, double y1);
        public native Stroke radialGradient(Array<String> colors, Array<Double> ratios, double x0, double y0, double r0, double x1, double y1, double r1);
    }

    public static class StrokeStyle {
        public String caps;
        public String joints;
        public double miterLimit;
        public double width;

        public StrokeStyle(double width, String caps, String joinds, double miterLimit) { /* compiled code */ }
    }
}
