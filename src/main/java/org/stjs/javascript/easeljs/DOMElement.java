package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Element;
import org.stjs.javascript.easeljs.DisplayObject;

@STJSBridge
@Namespace("createjs")
public class DOMElement extends DisplayObject {
    public Element htmlElement;
    public DOMElement(Element htmlElement) { /* compiled code */ }
}
