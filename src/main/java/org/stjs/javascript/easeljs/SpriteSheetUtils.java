package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.dom.Image;
import org.stjs.javascript.easeljs.SpriteSheet;

@STJSBridge
@Namespace("createjs")
public class SpriteSheetUtils {
    public static native Image extractFrame(SpriteSheet spriteSheet, int frame);
    public static native Image extractFrame(SpriteSheet spriteSheet, String animation);
}
