package org.stjs.javascript.easeljs;

import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;

@STJSBridge
@Namespace("createjs")
public class SpriteContainer extends Container {
    public SpriteSheet spriteSheet;

    public SpriteContainer() { /* compiled code */ }
    public SpriteContainer(SpriteSheet spriteSheet) { /* compiled code */ }
}
