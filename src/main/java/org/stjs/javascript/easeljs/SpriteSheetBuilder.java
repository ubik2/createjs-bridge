package org.stjs.javascript.easeljs;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.easeljs.DisplayObject;
import org.stjs.javascript.easeljs.MovieClip;
import org.stjs.javascript.easeljs.SpriteSheet;
import org.stjs.javascript.easeljs.Rectangle;
import org.stjs.javascript.functions.Callback0;
import org.stjs.javascript.functions.Callback1;
import org.stjs.javascript.functions.Callback2;
import org.stjs.javascript.functions.Callback3;
import org.stjs.javascript.functions.Callback4;
import org.stjs.javascript.functions.Function4;

@STJSBridge
@Namespace("createjs")
public class SpriteSheetBuilder {
    public double maxHeight;
    public double maxWidth;
    public double padding;
    public double progress;
    public double scale;
    public SpriteSheet spriteSheet;
    public double timeSlice;

    public SpriteSheetBuilder() { /* compiled code */ }
    public native void addAnimation(String name, Array<Integer> frames);
    public native void addAnimation(String name, Array<Integer> frames, String next);
    public native void addAnimation(String name, Array<Integer> frames, String next, int frequency);
    public native int addFrame(DisplayObject source);
    public native int addFrame(DisplayObject source, Rectangle sourceRect);
    public native int addFrame(DisplayObject source, Rectangle sourceRect, double scale);
    public native int addFrame(DisplayObject source, Rectangle sourceRect, double scale, Callback2<DisplayObject, Object> setupFunction);
    public native int addFrame(DisplayObject source, Rectangle sourceRect, double scale, Callback2<DisplayObject, Object> setupFunction, Object setupData);
    public native void addMovieClip(MovieClip source);
    public native void addMovieClip(MovieClip source, Rectangle sourceRect);
    public native void addMovieClip(MovieClip source, Rectangle sourceRect, double scale);
    public native void addMovieClip(MovieClip source, Rectangle sourceRect, double scale, Callback3<MovieClip, Object, Integer> setupFunction);
    public native void addMovieClip(MovieClip source, Rectangle sourceRect, double scale, Callback3<MovieClip, Object, Integer> setupFunction, Object setupData);
    public native void addMovieClip(MovieClip source, Rectangle sourceRect, double scale, Callback3<MovieClip, Object, Integer> setupFunction, Object setupData, Function4<String, MovieClip, Integer, Integer, String> labelFunction);
    public native SpriteSheet build();
    public native void buildAsync();
    public native void buildAsync(double timeSlice);
    public native void stopAsync();

}
